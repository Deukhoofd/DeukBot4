﻿using System;
using Npgsql;

namespace DeukBot4.Database
{
    public class DatabaseConnection : IDisposable
    {
        public static string ConnectionString { private get; set; }
        private readonly NpgsqlConnection _connection;

        public DatabaseConnection()
        {
            _connection = new NpgsqlConnection(ConnectionString);
            _connection.Open();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public static implicit operator NpgsqlConnection(DatabaseConnection conn)
        {
            return conn._connection;
        }
    }
}