using System.Collections.Generic;
using System.Threading.Tasks;
using DeukBot4.Utilities;
using Npgsql;

namespace DeukBot4.Database
{
    public static class WarnHandler
    {
        public static async Task AddWarning(ulong serverId, ulong userId, string message)
        {
            using var conn = new DatabaseConnection();
            await using var cmd = new NpgsqlCommand
            {
                Connection = conn,
                CommandText = "INSERT INTO warnings (serverId, userId, message)" +
                              "VALUES (@server_id, @user_id, @message);"
            };
            cmd.Parameters.AddWithValue("server_id", serverId.ToLong());
            cmd.Parameters.AddWithValue("user_id", userId.ToLong());
            cmd.Parameters.AddWithValue("message", message);
            await cmd.ExecuteNonQueryAsync();
        }
        
        public class Warning
        {
            public Warning(int id, string message)
            {
                Id = id;
                Message = message;
            }

            public int Id { get; }
            public string Message { get; }
        }

        public static async Task<List<Warning>> GetWarnings(ulong serverId, ulong userId)
        {
            using var conn = new DatabaseConnection();
            await using var cmd = new NpgsqlCommand
            {
                Connection = conn,
                CommandText = "SELECT id, message from warnings " +
                              "where serverId = @server_id and userId = @user_id;"
            };
            cmd.Parameters.AddWithValue("server_id", serverId.ToLong());
            cmd.Parameters.AddWithValue("user_id", userId.ToLong());
            var reader = cmd.ExecuteReader();
            var ls = new List<Warning>();
            while (await reader.ReadAsync())
            {
                var id = reader.GetInt32(0);
                var message = reader.GetString(1);
                ls.Add(new Warning(id, message));
            }
            return ls;
        }
    }
}