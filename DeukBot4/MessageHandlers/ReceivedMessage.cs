using Discord.WebSocket;

namespace DeukBot4.MessageHandlers
{
    public class ReceivedMessage
    {
        public readonly SocketMessage Message;
        public bool IsHandled;

        public ReceivedMessage(SocketMessage message)
        {
            Message = message;
            IsHandled = false;
        }
    }
}