using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DeukBot4.Database;
using DeukBot4.MessageHandlers.CommandHandler;
using DeukBot4.MessageHandlers.Permissions;
using Discord;

namespace DeukBot4.MessageHandlers
{
    public static class PointsMessageHandler
    {
        private static readonly Regex Matcher =
            new Regex(@"(?'amount'-?\d+) *points? *(?'unary'to|from) *(?:<@!*(?'id'\d+)>|(?'username'\w+))", RegexOptions.IgnoreCase);

        private static long MaxPoints(PermissionLevel level)
        {
            switch (level)
            {
                case PermissionLevel.Banned:
                case PermissionLevel.Bot:
                case PermissionLevel.Everyone:
                    return 0;
                case PermissionLevel.Helper:
                    return 10;
                case PermissionLevel.Moderator:
                    return 50;
                case PermissionLevel.Admin:
                    return 100;
                case PermissionLevel.ServerOwner:
                    return 1000;
                case PermissionLevel.BotCreator:
                    return long.MaxValue;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }

        public static async Task HandleMessage(ReceivedMessage receivedMessage)
        {
            if (!(receivedMessage.Message.Channel is IGuildChannel serverChannel))
                return;

            var permission = await PermissionValidator.GetUserPermissionLevel(receivedMessage.Message);
            if (permission < PermissionLevel.Helper)
                return;

            var match = Matcher.Match(receivedMessage.Message.Content);
            if (!match.Success)
                return;
            if (receivedMessage.IsHandled)
                return;
            receivedMessage.IsHandled = true;
            var message = receivedMessage.Message;

            var points = long.Parse(match.Groups["amount"].Value);

            var embed = EmbedFactory.GetStandardEmbedBuilder();
            embed.Title = "Points";

            var maxPoints = MaxPoints(permission);
            if (points > maxPoints)
            {
                embed.Description = $"You're not allowed to give that many points. Your max is {maxPoints}";
                message.Channel.SendMessageAsync("", embed: embed.Build());
                return;
            }

            var unary = match.Groups["unary"].Value;
            if (string.Equals(unary, "from"))
                points = -points;
            if (points == 0)
                return;
            IGuildUser user;
            if (match.Groups["id"].Success)
            {
                var userId = ulong.Parse(match.Groups["id"].Value);
                user = await serverChannel.Guild.GetUserAsync(userId);
            }
            else if (match.Groups["username"].Success)
            {
                var username = match.Groups["username"].Value;
                var users = await serverChannel.Guild.GetUsersAsync();
                user = users.FirstOrDefault(x =>
                    string.Equals(x.Username, username, StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                Logger.Main.LogError("Neither id nor username set");
                return;
            }

            if (user == null)
            {
                embed.Description = "Can't find that user";
                message.Channel.SendMessageAsync("", embed: embed.Build());
                return;
            }

            if (user.Id == message.Author.Id)
            {
                embed.Description = $"You're not allowed to give yourself points.";
                message.Channel.SendMessageAsync("", embed: embed.Build());
                return;
            }


            var newPoints = await PointHandler.ChangePoints(serverChannel.GuildId, user.Id, points);
            if (!newPoints.HasValue)
            {
                Logger.Main.LogError("Error");
                return;
            }
            embed.Description = points < 0
                ? $"{-points} points from {user.Mention}! Their total points is now {newPoints}!"
                : $"{points} points to {user.Mention}! Their total points is now {newPoints}!";
            message.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}