﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.Permissions;

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class Command
    {
        public Command(string                  name, PermissionLevel permission, string shortHelp,
            string                             longHelp,
            ParameterMatcher.ParameterType[][] parameterTypes, bool forbidInPm, bool requireParameterMatch,
            MethodInfo                         function,
            CommandContainerBase               commandContainer)
        {
            Name                  = name;
            Permission            = permission;
            ShortHelp             = shortHelp;
            LongHelp              = longHelp;
            Function              = function;
            CommandContainer      = commandContainer;
            ParameterTypes        = parameterTypes;
            HasHelp               = true;
            ForbidInPm            = forbidInPm;
            RequireParameterMatch = requireParameterMatch;
        }

        public Command(string                  name, PermissionLevel permission,
            ParameterMatcher.ParameterType[][] parameterTypes,
            bool                               forbidInPm, bool                 requireParameterMatch,
            MethodInfo                         function,   CommandContainerBase commandContainer)
        {
            Name                  = name;
            Permission            = permission;
            Function              = function;
            CommandContainer      = commandContainer;
            ParameterTypes        = parameterTypes;
            HasHelp               = false;
            ForbidInPm            = forbidInPm;
            RequireParameterMatch = requireParameterMatch;
        }

        public string                             Name                  { get; }
        public List<string>                       Alternatives          { get; } = new List<string>();
        public PermissionLevel                    Permission            { get; }
        public string                             ShortHelp             { get; }
        public string                             LongHelp              { get; }
        public MethodInfo                         Function              { get; }
        public CommandContainerBase               CommandContainer      { get; }
        public bool                               HasHelp               { get; }
        public ParameterMatcher.ParameterType[][] ParameterTypes        { get; }
        public bool                               ForbidInPm            { get; }
        public bool                               RequireParameterMatch { get; }

        private string[] _parameterMatchers;

        public string[] ParametersMatchers =>
            _parameterMatchers ?? (_parameterMatchers = ParameterMatcher.GenerateRegex(this));

        public async Task Invoke(CommandRequest request)
        {
            await (Task) Function.Invoke(CommandContainer, new object[] {request});
        }
    }
}