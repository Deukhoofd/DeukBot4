using System.Collections.Generic;
using System.Threading.Tasks;
using DeukBot4.Database.ServerSettings;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;
using DeukBot4.MessageHandlers.JokeHandling;
using DeukBot4.MessageHandlers.Permissions;
using Discord;
#pragma warning disable 4014

namespace DeukBot4.MessageHandlers.CommandHandler
{
    public class JokeControlCommands : CommandContainerBase
    {
        public override string Name => "Joke Control";

        [Command("togglejoke", PermissionLevel.Admin)]
        [CommandParameters(ParameterMatcher.ParameterType.Remainder)]
        [CommandHelp("Toggle a joke on or off", "Toggle a joke, defined by id, on or off")]
        [BlockUsageInPm, RequireParameterMatch]
        public async Task ToggleJoke(CommandRequest request)
        {
            var jokeName = request.Parameters[0].AsString();
            if (!JokeHandler.Jokes.ContainsKey(jokeName))
            {
                request.SendSimpleEmbed("Joke Toggle", $"Not a valid joke id: {jokeName}");
                return;
            };
            if (request.OriginalMessage.Channel is IGuildChannel guildChannel)
            {
                var serverSettings = ServerSettingHandler.GetSettings(guildChannel.GuildId);
                if (serverSettings.EnabledJokes.Contains(jokeName))
                {
                    serverSettings.EnabledJokes.Remove(jokeName);
                    serverSettings.SetEnabledJokes(serverSettings.EnabledJokes);
                    request.SendSimpleEmbed("Joke Toggle", $"Disabled Joke with id {jokeName}");
                }
                else
                {
                    serverSettings.EnabledJokes.Add(jokeName);
                    serverSettings.SetEnabledJokes(serverSettings.EnabledJokes);
                    request.SendSimpleEmbed("Joke Toggle", $"Enabled Joke with id {jokeName}");
                }
            }
        }

        [Command("jokes", PermissionLevel.Admin)]
        [CommandHelp("Lists jokes per server", "Lists jokes per server")]
        [BlockUsageInPm]
        public async Task ListJokes(CommandRequest request)
        {
            if (request.OriginalMessage.Channel is IGuildChannel guildChannel)
            {
                var eb = EmbedFactory.GetStandardEmbedBuilder();
                var serverSettings = ServerSettingHandler.GetSettings(guildChannel.GuildId);
                foreach (var jokesKey in JokeHandler.Jokes.Keys)
                {
                    if (serverSettings.EnabledJokes.Contains(jokesKey))
                    {
                        eb.AddField(jokesKey, "Enabled", true);
                    }
                    else
                    {
                        eb.AddField(jokesKey, "Disabled", true);
                    }
                }

                request.SendMessageAsync("", embed: eb.Build());
            }
        }

        [Command("nofunallowed", PermissionLevel.Admin)]
        [CommandHelp("Disables all jokes", "Disables all jokes")]
        [BlockUsageInPm]
        public async Task DisableJokes(CommandRequest request)
        {
            if (request.OriginalMessage.Channel is IGuildChannel guildChannel)
            {
                var serverSettings = ServerSettingHandler.GetSettings(guildChannel.GuildId);
                serverSettings.SetEnabledJokes(new List<string>());
                request.SendSimpleEmbed("Joke Toggle", $"Disabled all jokes");
            }
        }
    }
}