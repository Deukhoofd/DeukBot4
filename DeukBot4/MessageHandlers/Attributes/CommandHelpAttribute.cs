﻿using System;

namespace DeukBot4.MessageHandlers
{
    public class CommandHelpAttribute : Attribute
    {
        public string ShortHelp { get; }
        public string LongHelp { get; }

        public CommandHelpAttribute(string shortHelp, string longHelp)
        {
            ShortHelp = shortHelp;
            LongHelp = longHelp;
        }
    }
}