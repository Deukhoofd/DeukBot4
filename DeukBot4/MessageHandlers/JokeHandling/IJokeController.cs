using System.Threading.Tasks;
using JetBrains.Annotations;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    [UsedImplicitly]
    public interface IJokeController
    {
        string Id { get; }
        string Name { get; }

        Task Run(ReceivedMessage message);
    }
}