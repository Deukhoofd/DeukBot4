﻿using System.Text;

namespace DeukBot4.Utilities
{
    public static class StringExtensions
    {
        public static string RemoveSpecialCharacters(this string str) {
            var sb = new StringBuilder();
            foreach (var c in str) {
                if (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c)) {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static StringBuilder TrimEnd(this StringBuilder sb)
        {
            if (sb == null || sb.Length == 0) return sb;

            int i = sb.Length - 1;
            for (; i >= 0; i--)
                if (!char.IsWhiteSpace(sb[i]))
                    break;

            if (i < sb.Length - 1)
                sb.Length = i + 1;

            return sb;
        }
    }
}